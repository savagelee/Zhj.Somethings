﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApiClient;
using Zhj.Common.HystrixCore;
using Zhj.Somethings.OrderApi.Api;
using Zhj.Somethings.OrderApi.Dto;

namespace Zhj.Somethings.OrderService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private Service.OrderService orderService { set; get; }
        public OrderController(Service.OrderService orderService)
        {
            this.orderService = orderService;
        }


        [HttpGet("GetOrderList")]
        public IList<OrderDto> GetOrderList()
        {
            Console.WriteLine("访问了订单列表" + DateTime.Now);
            return orderService.GetOrderList();
        }



    }
}
