﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WebApiClient;
using WebApiClient.Attributes;
using Zhj.Somethings.OrderApi.Dto;

namespace Zhj.Somethings.OrderApi.Api
{
    public interface OrderServiceApi: IHttpApi
    {
        
        [HttpGet("/OrderService/Order/GetOrderList")]
        ITask<IList<OrderDto>> GetOrderList();

    }
}