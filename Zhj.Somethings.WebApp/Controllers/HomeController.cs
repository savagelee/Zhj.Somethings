﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApiClient;
using Zhj.Common.Logger;
using Zhj.Somethings.OrderApi.Api;
using Zhj.Somethings.OrderApi.Dto;
using Zhj.Somethings.WebApp.Models;

namespace Zhj.Somethings.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ILogger Logger { get; }
        
        public OrderServiceApi orderServiceApi { get; set; }

        public HomeController(OrderServiceApi orderServiceApi, ILogger logger)
        {
            this.orderServiceApi = orderServiceApi; 
            Logger = logger;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet()]
        public ITask<IList<OrderDto>> GetList()
        {
            Logger.Info("测试的 访问了订单接口", "Tag1", "Tag2");
            return orderServiceApi.GetOrderList();
        }

    }
}
