﻿using System;
using System.Collections.Generic;
using System.Text;
using Zhj.Somethings.SampleService.Domain.Entity;
using System.Linq;
using Zhj.Somethings.SampleService.Domain.Cope;

namespace Zhj.Somethings.SampleService.Application.Cope
{
    public class CopeApplicationImpl : ICopeApplication
    {
        public ICopeService _copeService { set; get; }
        public CopeApplicationImpl(ICopeService copeService)
        {
            _copeService = copeService;
        }

        public List<Product> GetProducts()
        {
            return _copeService.GetProducts();
        }
    }
}
