﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zhj.Somethings.SampleService.Domain.IRepository
{
    public interface IBaseRepository<TEntity, in TPrimaryKey> where TEntity : class
    {
        bool Add(TEntity T);

        bool Delete(TPrimaryKey id);

        bool Update(TEntity T);

        TEntity Get(TPrimaryKey id);

        //List<TEntity> Get(Expression<Func<TEntity, bool>> lmd);

        List<TEntity> GetAll();
    }
}
