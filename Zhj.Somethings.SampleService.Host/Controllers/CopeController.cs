﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zhj.Somethings.SampleService.Application.Cope;
using Zhj.Somethings.SampleService.Domain.Entity;

namespace Zhj.Somethings.SampleService.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CopeController : ControllerBase
    {
        private ICopeApplication _copeApplication { set; get; }
        public CopeController(ICopeApplication copeApplication)
        {
            this._copeApplication = copeApplication;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            return _copeApplication.GetProducts();
        }

    }
}